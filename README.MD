# Notation automatique projet L3POO
Ce repository contient le code necessaire a la notation
automatique du TP note pour les L3POO.
Le code devra etre integre dans Moodle...

## Tests realises
Sous Linux avec JDK Openjdk8u222
33 tests passent actuellement...
## Ce qui est fait 
Implementation sous forme de tests unitaires des sujets de validation 
proposes par @Anne Vialard dans son mail. Chaque item correspondant a un test unitaire
annote par @Mark pour le nombre de points attribues...
Acuellement implementes 33 tests ...

## Ce qui n'a pas ete fait
Il manque encore:

- finalisation du test  de la methode getTotalCost()
- finalisation du test de la compilation avec sources fournies aux etudiants
(gestion des ensembles a ecraser et ceux a conserver)
- adapter les scripts:
  - enlever  l'appel a Javac devenu inutile (deja fait?)
  - ajouter la dependance qdox (deja fait?)
  - ces taches sont a faire par @Vincent
- lancement sur tous les projets etudiants


# Utiliser le projet
Un fichier grade.properties permet d'ajuster l'emplacement des sources:
- une entree *src.student.path* correspond a l'emplacement des sources uploadees par l'etudiant
- l'autre entree *src.provided.path* correspond a l'emplacement des sources fournies aux etudiants
*src.student.path* permet de trouver les sources  pour la librairie QDox dans tous tests
sauf dans le cas du test de la compilation avec les sources fournies.
 
 Lancer le programme AutomaticGradingRunner en lieu et place de l'ancien
 apres avoir renseigne le fichier grade.properties et la notation sera faite automatiquement.
 Les points enleves sont signales (test KO) et le nom du test ainsi que le message d'echec...