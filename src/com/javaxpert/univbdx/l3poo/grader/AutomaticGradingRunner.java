package com.javaxpert.univbdx.l3poo.grader;


import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import java.util.stream.Stream;
import static java.lang.reflect.Modifier.*;

public class AutomaticGradingRunner {
    public static void main(String[] args) {
        System.out.println("Launching automatic grading...");
        // compute the maximum score using annotations
        // retains the only public and non static methods first
        // then get the ones annotated with @Test
        Class c = AutomaticGrade.class;
        int max_score = Stream.of(c.getMethods())
                .filter(m -> {
                    return (isPublic(m.getModifiers()) && !isStatic(m.getModifiers()));
                            //  .filter(m.getAnnotation(Test.class))

                })
                .filter(m-> m.getAnnotation(Test.class)!=null)
                .map(m -> m.getAnnotation(Mark.class).value())
                .reduce(0, (m1, m2) -> m1 + m2);


        // launch the runner
        JUnitCore runner = new JUnitCore();
        Result result = runner.run(AutomaticGrade.class);
        int  sum = result.getFailures().stream()
                .filter(f -> {
                    System.out.println("Failed test is :" + f.toString() + " Reason : " + f.getMessage());
                    return true;
                })
                .map(Failure::getDescription)
                .map(desc -> desc.getAnnotation(Mark.class).value())
                .reduce(0,
                        (v1,v2) -> v1+v2);
        System.out.println("Computed sum of lost points with " + result.getFailures().size() + " failed tests is = " + sum);
        int grade = max_score - sum;
        System.out.println("Final score is :" + grade);
    }
}
