package com.javaxpert.univbdx.l3poo.grader;

import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaConstructor;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import javax.tools.*;
import static org.junit.Assert.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class AutomaticGrade {
    // where lives the files uploaded by student
    private final static String STUDENT_SRC_PATH="src.student.path";

    // where lives default files provided to student
    private final static String PROVIDED_SRC_PATH="src.provided.path";

    // used to retrieve Java Classes from source files
    private static JavaProjectBuilder builder;

    private static Properties props;
    @BeforeClass
    public static void setup(){
        props = new Properties();
        try {
            props.load(new FileInputStream("grade.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        builder = new JavaProjectBuilder();
        builder.addSourceTree(
                new File(props.getProperty(STUDENT_SRC_PATH)));


    }

    private JavaClass fetchClassByName(String name){
        return builder.getClasses().stream()
                .filter(javaClass -> {
                    //System.err.println("Current class is = " + javaClass.toString() + " Canonical  name = " + javaClass.getCanonicalName() + " Simple name : "+ javaClass.getName());
                    //System.err.flush();
                    return javaClass.getCanonicalName().contains(name.trim());
                })
                .collect(Collectors.toList()).get(0);
    }
    private JavaMethod getMethodFromClass(String classname,String methodname){
        JavaClass target_class = fetchClassByName(classname);
        JavaMethod method = target_class.getMethods().stream()
                .filter(m -> m.getName().equals(methodname))
                .collect(Collectors.toList())
                .get(0);
        return method;
    }
    private List<JavaConstructor> getConstructorsFromClass(String classname){
        JavaClass target_class = fetchClassByName(classname);
        return target_class.getConstructors();
    }

    private List<JavaField> getFieldsFromClass(String classname){
        JavaClass target_class = fetchClassByName(classname);
        return target_class.getFields();
    }

    @Test
    @Mark(1)
    public void isActivityPresent(){
        JavaClass activity_class = fetchClassByName("activity.Activity");
        Assert.assertTrue(activity_class!=null );
    }

    @Test
    @Mark(1)
    public void  activityContainsGetCost(){
        JavaMethod method = getMethodFromClass("activity.Activity","getCost");
        assertTrue(method!=null && method.isAbstract());
    }

    @Test
    @Mark(1)
    public void activityContainsOneConstructor(){
        assertTrue(getConstructorsFromClass("activity.Activity").stream()
        .filter(c -> c.getParameters().size()==1 )
                .collect(Collectors.toList())
                .get(0) != null
        );
    }

    @Test
    @Mark(1)
    public void activityContainsExpectedFields(){
        List<JavaField> fields = getFieldsFromClass("activity.Activity");
        int number_fields = fields.stream().filter(f -> f.getName().equals("hours") && (f.isPrivate() || f.isProtected() ) )
                        .collect(Collectors.toList()).size();
        assertTrue(number_fields>=1);
    }

    @Test
    @Mark(1)
    public void  labExtendsActivity(){
        JavaClass lab = fetchClassByName("activity.Lab");
        assertTrue(lab!=null && lab.getSuperClass().getCanonicalName().equals("activity.Activity"));
    }

    @Test
    @Mark(1)
    public void labContainsMaxStudentsField(){
        List<JavaField> fields = getFieldsFromClass("activity.Lab");
        assertNotNull(fields.stream()
                .filter(f -> (f.getName().equals("nbStudentsMax") && f.isPrivate()))
                .collect(Collectors.toList()).get(0));
    }

    @Test
    @Mark(2)
    public void labContainsContructorWith2Params(){
        assertTrue(
                getConstructorsFromClass("activity.Lab").stream()
                        .filter (c -> {
                            System.err.println (c);
                            return true;
                        })
                .filter(c -> {
                            System.out.println("Constructor : " + c);
                            System.out.flush();
                            return c.getParameters().size() == 2;
                        }
                )
                        .filter(c -> {
                            System.err.println(" Constructor : " + c.toString() + " First param type :" + c.getParameters().get(0).getType());
                            return c.getParameters().get(0).getType().getCanonicalName().equals("int");
                        })
                        .filter(c -> {
                            System.err.println(" Constructor : " + c.toString() + " Second param type :" + c.getParameters().get(1).getType());
                            return c.getParameters().get(1).getType().getCanonicalName().equals("int");
                        })

                .collect(Collectors.toList()).size()== 1);
        ;
    }

    @Test
    @Mark(1)
    public void labContainsGetCost(){
        JavaMethod method = getMethodFromClass("activity.Lab","getCost");
        assertTrue(method!=null && !method.isAbstract());
    }


    @Test
    @Mark(1)
    public void labContainsToString(){
        JavaMethod method = getMethodFromClass("activity.Lab","toString");
        assertTrue(method!=null && method.isPublic());
    }

    @Test
    @Mark(1)
    public void lectureHasNoField(){
        List<JavaField> fields = getFieldsFromClass("activity.Lecture");
        assertTrue(fields.isEmpty());
    }

    @Test
    @Mark(1)
    public void lectureExtendsActivity(){
        JavaClass lecture = fetchClassByName("activity.Lecture");
        assertTrue(lecture!=null && lecture.getSuperClass().getCanonicalName().equals("activity.Activity"));
    }

    @Test
    @Mark(1)
    public void courseContainsConstructorWith3Params(){
        assertTrue(
        getConstructorsFromClass("course.Course").stream()
                .filter( c -> c.getParameters().size() == 3 )
            .count() >1);
    }


    @Test
    @Mark(1)
    public void courseHasOnePrivateField(){
        List<JavaField> fields = getFieldsFromClass("course.Course");
        assertTrue(fields.stream()
                .filter (f -> (f.isPrivate() && f.getName().equals("title")))
                .collect(Collectors.toList()).size()==1 );
    }

    @Test
    @Mark()
    public void courseHasGetTitle(){
        JavaMethod method = getMethodFromClass("course.Course","getTitle");
        assertTrue(method!=null && method.isPublic() && method.getReturnType().getCanonicalName().equals("java.lang.String"));
    }

    @Test
    @Mark(1)
    public void courseContainsToString(){
        JavaMethod method = getMethodFromClass("course.Course","toString");
        assertTrue(method!=null && method.isPublic() && method.getReturnType().getCanonicalName().equals("java.lang.String"));
    }

    @Test
    @Mark(1)
    public void programHasExpectedConstructor(){
        assertTrue(getConstructorsFromClass("course.Program").stream()
                .filter( c -> c.getParameters().size() == 3 )
                .filter( c -> c.getParameters().get(0).getType().getCanonicalName().equals("java.lang.String"))
                .filter(c -> c.getParameters().get(1).getType().getCanonicalName().equals("int"))
                .filter(c -> c.getParameters().get(2).getType().getCanonicalName().equals("int"))
                .count()>=1);
    }

    @Test
    @Mark(1)
    public void programHasTitleField(){
        assertTrue(
                getFieldsFromClass("course.Program").stream()
                .filter(f -> f.getName().equals("title"))
                        .filter( f-> f.getType().getCanonicalName().equals("java.lang.String"))
                .count()==1)
        ;
    }
    @Test
    @Mark(1)
    public void programHasNumberStudentsField(){
        assertTrue(
                getFieldsFromClass("course.Program").stream()
                        .filter(f -> f.getName().equals("nbStudents"))
                        .filter( f-> f.getType().getCanonicalName().equals("int"))
                        .count()==1)
        ;
    }

    @Test
    @Mark(1)
    public void programHasMaxCostField(){
        assertTrue(
                getFieldsFromClass("course.Program").stream()
                        .filter(f -> f.getName().equals("maxCost"))
                        .filter( f-> f.getType().getCanonicalName().equals("int"))
                        .count()==1)
        ;
    }

    @Test
    @Mark(1)
    public void programHasArrayOfCourses(){
        assertTrue(
                getFieldsFromClass("course.Program").stream()
                        .filter( f-> f.isPrivate())
                        .filter( f-> {
                            System.out.println("current Field = "+ f.getName() + " Type = "+ f.getType().toString());
                            return f.getType().getCanonicalName().equals("course.Course[]");
                        })
                        .count()>=1)
        ;
    }

    @Test
    @Mark(1)
    public void programHasOneOtherField(){
        assertTrue(
                getFieldsFromClass("course.Program").stream()
                        .filter( f-> f.isPrivate())
                        .filter( f-> f.getType().getCanonicalName().equals("int"))
                .filter(f -> (!f.getName().equals("nbStudents") && !f.getName().equals("maxCost")))
                        .count()>=1);
    }

    @Test
    @Mark(1)
    public void programContainsAddCourseMethod(){
        JavaMethod method = getMethodFromClass("course.Program","addCourse");
        assertTrue(
                method!=null && method.isPublic()
        );
    }

    @Test
    @Mark(1)
    public void programAddCourseRaiseExpectedException(){
        JavaMethod method = getMethodFromClass("course.Program","addCourse");
        assertTrue(method!=null );
        assertTrue(method.getExceptionTypes().size()>0);
        assertTrue( method.getExceptionTypes().stream()
            .filter(e -> e.getCanonicalName().equals("course.MaxSizeExceededException"))
                .count()==1
        );
    }

    @Test
    @Mark(1)
    public void programContainsGetTotalCost(){
        JavaMethod method = getMethodFromClass("course.Program","getTotalCost");
        assertTrue(method!=null && method.isPublic() && method.getParameters().size()==0);
    }

    @Test
    @Mark(1)
    public void programContainsGetTitle(){
        JavaMethod method = getMethodFromClass("course.Program","getTitle");
        assertTrue(method.isPublic() && method.getReturnType().getCanonicalName().equals("java.lang.String"));
    }

    @Test
    @Mark(1)
    public void programContainsGetNumberStudents(){
        JavaMethod method = getMethodFromClass("course.Program","getNbStudents");
        assertTrue(method.isPublic() && method.getReturnType().getCanonicalName().equals("int"));
    }

    @Test
    @Mark(1)
    public void programContainsGeMaxCost(){
        JavaMethod method = getMethodFromClass("course.Program","getMaxCost");
        assertTrue(method.isPublic() && method.getReturnType().getCanonicalName().equals("int"));
    }

    @Test
    @Mark(1)
    public void exceptionIsDefined(){
        JavaClass maxsizeexc= builder.getClassByName("course.MaxSizeExceededException");
        assertTrue(maxsizeexc!=null && maxsizeexc.isPublic() && maxsizeexc.getSuperClass().getCanonicalName().equals("java.lang.Exception"));
    }

    @Test
    @Mark(1)
    public void exceptionHasOnePrivateIntegerField(){
        assertTrue(getFieldsFromClass("course.MaxSizeExceededException").stream()
                .filter(f->f.isPrivate())
                .filter(f-> f.getType().getCanonicalName().equals("int"))
                .count()>=1);
    }

    @Test
    @Mark(1)
    public void exceptionHasOneGetter(){
        JavaMethod method = getMethodFromClass("course.MaxSizeExceededException","getSize");
        assertTrue(method!=null && method.isPublic() && method.getReturnType().getCanonicalName().equals("int"));
    }

    @Test
    @Mark(1)
    public void exeptionHasExpectedConstructor(){
        assertTrue(getConstructorsFromClass("course.MaxSizeExceededException").stream()
                .filter( c -> c.getParameters().size() >= 1 )
                .filter( c -> c.getParameters().stream()
                        .filter(p -> p.getType().getCanonicalName().equals("int"))
                        .collect(Collectors.toList()).size()>0
                )
                .count()>=1);
    }

    @Test
    @Mark(3)
    public  void compileWithUploadedSources(){
        // setup a Java compiler environent
        // 1- get a compiler
        // 2 - setup files to be compiled
        // 3 - defines a file manager & diagnostics
        // 4 - launch the
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics =
                new DiagnosticCollector<>();

        // get all classes fetched by QDOX
        // then filter them (drops all classes for non test code)
        Collection<JavaClass> all_classes = builder.getClasses();

        all_classes.stream().forEach(System.out::println);
        Stream<JavaClass> stream_classes = all_classes.stream();
        Set<JavaClass> filtered_classes =  stream_classes.filter(
                x -> (x.getPackage().getName().startsWith("com.javaxpert")!=true)
        ).collect(Collectors.toSet());


        Set<File> java_files = filtered_classes.stream()
                .map( java -> new File(java.getSource().getURL().getPath()))
                .collect(Collectors.toSet());

        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics,null,null);
        Iterable<? extends JavaFileObject> javaFileObjects =
                fileManager.getJavaFileObjects(java_files.toArray(new File[java_files.size()]));
        JavaCompiler.CompilationTask task = compiler.getTask(
                null, fileManager, diagnostics, null,
                null, javaFileObjects);
        System.out.println("Compiling : "+ java_files.size() + " Java source files");
        Boolean compil_result = task.call();
        System.out.println("Compilation succeeded ?"+ compil_result);
        assertTrue("Compilation succeeded?",compil_result==true);

    }

    @Test
    @Mark(3)
    public  void compileWithOriginalSources(){
        // builder now uses the sources as provided to students
        // while other tests use the sources as uploaded by students
        JavaProjectBuilder builder = new JavaProjectBuilder();
        builder.addSourceTree(new File(props.getProperty(PROVIDED_SRC_PATH)));
        // setup a Java compiler environent
        // 1- get a compiler
        // 2 - setup files to be compiled
        // 3 - defines a file manager & diagnostics
        // 4 - launch the
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics =
                new DiagnosticCollector<>();

        // get all classes fetched by QDOX
        // they do not contain classes used by automatic grading
        Collection<JavaClass> all_classes = builder.getClasses();
        Set<File> java_files = all_classes.stream()
                .map( java -> new File(java.getSource().getURL().getPath()))
                .collect(Collectors.toSet());
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics,null,null);
        Iterable<? extends JavaFileObject> javaFileObjects =
                fileManager.getJavaFileObjects(java_files.toArray(new File[java_files.size()]));
        JavaCompiler.CompilationTask task = compiler.getTask(
                null, fileManager, diagnostics, null,
                null, javaFileObjects);
        System.out.println("Compiling : "+ java_files.size() + " Java source files");
        Boolean compil_result = task.call();
        System.out.println("Compilation succeeded ?"+ compil_result);
        assertTrue("Compilation succeeded?",compil_result==true);
    }


//    @Test
//    @Mark(3)
//    // TODO finish this test
//    public void isCourseGetTotalCostOk(){
//        try {
//            Class program_class = Class.forName("course.Program");
//            Class lecture_class = Class.forName("activity.Lecture");
//            Class lab_class = Class.forName("activity.Lab");
//            Class course_class = Class.forName("course.Course");
//
//            Constructor program_constructor = program_class.getConstructor(String.class,int.class,int.class);
//            Constructor lecture_constructor = lecture_class.getConstructor(int.class);
//            Constructor lab_constructor = lab_class.getConstructor(int.class,int.class);
//            Constructor course_constructor1 = course_class.getConstructor(String.class,int.class,int.class);
//            Constructor course_constructor2 = course_class.getConstructor(String.class, Activity.class);
//            Constructor course_constructor3 = course_class.getConstructor(String.class,Activity.class,Activity.class);
//            Program p = (Program) program_constructor.newInstance("Program 1", 200, 180);
//            Activity lect1 = (Activity) lecture_constructor.newInstance(16);
//            Activity lect2 = (Activity) lecture_constructor.newInstance(24);
//            Activity lab1 =(Activity) lab_constructor.newInstance(32,20);
//            Activity lab2 =(Activity) lab_constructor.newInstance(16,25);
//            Course c0= (Course) course_constructor3.newInstance("Java programming", lect2, lab1);
//            Course c1= (Course) course_constructor2.newInstance("Algorithms", lect1);
//            Course c2 = (Course)course_constructor2.newInstance("Open source software", lab1);
//            Course c3 = (Course)course_constructor3.newInstance("Web programming", lect2, lab2);
//
//            p.addCourse(c0);
//            p.addCourse(c1);
//            p.addCourse(c2);
//            p.addCourse(c3);
//
//            int total = p.getTotalCost();
//            System.out.println("getTotalCost is over, total is " + total );
//
//            //assertEquals("Computed total should be equal to ",total);
//        }catch(Exception e){
//            e.printStackTrace();
//            System.err.println("Exception ocuured while invoking Class.forName"+ e.getMessage() + " : " + e.getCause());
//            fail("Unable to handle the Program class through reflection");
//        }
//        assertEquals(true,false);
//
//    }
}
