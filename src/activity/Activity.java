package activity;

public abstract class Activity {
    protected int hours;

    public Activity(int hours) {
        this.hours = hours;
    }

    public Activity(int hours,int other){
        this(hours);
        // other stuff goes here
    }

    public String toString() {
        return getType() + "(" + hours + "h)";
    }

    public abstract String getType();

    public abstract int getCost(int nbStudents);

}